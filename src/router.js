import React from 'react';
import {Router, Route, Switch} from 'dva/router';
import {ROUTER} from './constants';

import Share from "./routes/Share.js";

function RouterConfig({history}) {
  return (
    <Router history={history}>
      <Route path="/share" component={Share} />
    </Router>
  );
}

export default RouterConfig;



{/*<Switch>*/}
{/*</Switch>*/}
