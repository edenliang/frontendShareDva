import React, {Component} from 'react';
import styles from './Before.css'
import {} from 'rc-tween-one';
import GSAP from 'react-gsap-enhancer';
import {TweenMax,TimelineMax} from 'gsap';

class Before extends Component {

  init() {
    const {turn} = this.props.share;
    let opacity = 0;
    if (1 <= turn && turn <= 2)
      opacity = 1;
    // TweenMax.set("#before", {x: turn * 100, opacity});

    console.log(this.refs);


  }

  act() {
    const {turn} = this.props.share;
    let opacity = 0;
    if (1 <= turn && turn <= 2)
      opacity = 1;
    // return TweenMax.to("#before", 1, {x: 100 * turn, opacity, ease: Linear.easeInOut});
    if (turn == 1) {
      TweenMax.to("#react", 1, {width: "100%", opacity, ease: Linear.easeInOut});
      TweenMax.to("#redux", 1, {opacity: 0})
    }
    if (turn == 2) {
      // TweenMax.to("#before", 1, {x: 100 * turn, opacity, ease: Linear.easeInOut});
      TweenMax.to("#react", 1, {width: "50%", opacity, ease: Linear.easeInOut});
      TweenMax.to("#redux", 1, {opacity: 1, delay: 1})
    }
    console.log(this.refs);


  }

  componentDidMount() {
    // this.act = act(this.props.share.turn);
    this.init();

  }


  render() {
    const {turn} = this.props.share;
    console.log(turn,TimelineMax);
    this.act();
    return (
      <div id="before" className={styles.normal}>
        <div id="react" className={styles.react}>react</div>
        <div id="redux" className={styles.redux}>redux</div>
        <div id="saga" className={styles.saga}>saga</div>
        <div className={styles.dva}>dva</div>
      </div>
    );
  }
}

export default Before;
