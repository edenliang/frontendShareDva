import dva from 'dva';
import createHistory from 'history/createBrowserHistory';
import createLoading from 'dva-loading';

const app = dva({
  history: createHistory(),
});

app.use(createLoading());

app.model(require("./models/share"));

app.router(require('./router'));

app.start('#root');
