import React from 'react';
import {connect} from 'dva';
import styles from './Share.css';
import {BeforeComponent, AfterComponent} from '../components/share';


function Share({share}) {
  const {turn} = share;
  return (
    <div className={styles.normal}>
      <div className={styles.title}>框架演变</div>
      {1 <= turn ? <BeforeComponent share={share}/> : null}
      <AfterComponent share={share}/>


    </div>
  );
}

function mapStateToProps({share}) {
  return {share};
}

export default connect(mapStateToProps)(Share);
