import key from 'keymaster';

export default {
  namespace: 'share',
  state: {
    turn: 0
  },
  reducers: {
    before(state, {payload}){
      return {...state, turn: state.turn == 0 ? state.turn : state.turn - 1};
    },
    next(state, {payload}){
      return {...state, turn: state.turn + 1};
    }
  },
  effects: {},
  subscriptions: {
    keyEvent({dispatch}) {
      key('up', () => {
        dispatch({type: 'before'})
      });
      key('down', () => {
        dispatch({type: 'next'})
      })
    }
  },
};
